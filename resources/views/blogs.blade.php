<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <!-- Latest compiled and minified CSS & JS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <meta id="token" name="csrf-token" content="{{ csrf_token() }}" />

    </head>
    <body id="app">
    <br>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="#" @submit.prevent="addNewPost" method="post">
                        <legend>New Post</legend>

                        <div class="form-group">
                            <label for="">Title</label>
                            <input v-model="newPost.title" type="text" class="form-control" id="" placeholder="Input field">
                            <label for="">Body</label>
                            <textarea v-model="newPost.body" class="form-control" id="" placeholder="Input field"></textarea> 
                        </div>

                        <button type="submit" class="btn btn-success">Add post</button>
                        <a href="#" class="btn btn-default" @click="editPost(newPost.id)">Edit post</a>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <table class="panel-body table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Body</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr  v-for="blog in blogs">
                            <td>@{{ blog.title }}</td>
                            <td>@{{ blog.body   }}</td>
                            <td>@{{ blog.created_at   }}</td>
                            <td>@{{ blog.updated_at   }}</td>
                            <td>
                                <button type="button" class="btn btn-default" @click="showPost(blog.id)">Edit</button>
                                <button type="button" class="btn btn-danger" @click="removePost(blog.id)">Remove</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js" crossorigin="anonymous"></script>
        <script src="//code.jquery.com/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
        <script>
            new Vue({
                http: {
                    headers: {
                        'X-CSRF-TOKEN': $('#token').attr('content')
                    }
                },

                el: '#app',
                    data: {
                        blogs: [],
                        newPost: {
                            id: '',
                            title: '',
                            body:  ''
                        }
                    },
                    ready() {
                        this.fetchBlogs();
                    },

                    methods: {
                        /* Actualizar lista de post*/
                        fetchBlogs() {
                            // GET /someUrl
                            this.$http.get('api/blogs').then((response) => {
                                    this.$set('blogs', response.json())
                                }, (response) => {
                                      // error callback
                            });
                        },

                        addNewPost()
                        {
                            var post = this.newPost;

                            this.newPost = { title: '', body: ''};

                            this.$http.post('api/blogs',post).then((response) => {
                                    this.fetchBlogs();
                                }, (response) => {
                                      // error callback
                            });
                        },

                        showPost(id)
                        {
                            this.$http.get('api/blogs/' + id).then((data) => {
                                    this.$set('newPost', data.json());
                                }, (response) => {
                                      // error callback
                            });
                        },

                        editPost(id)
                        {
                            var post = this.newPost;

                            this.newPost = { title: '', body: ''};
                            this.$http.put('api/blogs/' + post.id, post).then((data) => {

                                this.fetchBlogs();

                                }, (response) => {
                                      // error callback
                            });
                        },

                        removePost(id)
                        {
                            this.$http.delete('api/blogs/' + id).then((data) => {
                                this.fetchBlogs();
                                }, (response) => {
                                      // error callback
                            });
                        }
                    }
                });

        </script>
    </body>
</html>
