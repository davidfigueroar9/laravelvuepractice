<?php

namespace App\Contracts;

interface DateHandler
{
	public function today();

	public function diffForHumans();
}
