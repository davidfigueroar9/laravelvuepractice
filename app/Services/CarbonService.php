<?php

namespace App\Services;

use App\Contracts\DateHandler;
use Carbon\Carbon;

class CarbonService implements DateHandler
{
	protected $carbon;

	public function __construct(Carbon $carbon)
	{
		$this->carbon = $carbon;

	}

	public function today()
	{
		return $this->carbon->today();
	}

	public function diffForHumans()
	{
		return $this->carbon->today()->subDay(2)->diffForHumans();
	}
}
