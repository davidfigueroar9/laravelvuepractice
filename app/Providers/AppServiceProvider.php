<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\DateHandler;
use App\Services\CarbonService;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DateHandler::class, function ($app) {
            Carbon::setlocale('es');
            return new CarbonService(new Carbon);
        });
    }
}
